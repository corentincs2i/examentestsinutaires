package Ex2;

import java.util.HashMap;

public class InventoryManager {
    private HashMap<String, Integer> inventory;

    /**
     * Constructeur de la classe InventoryManager. inventory = nouvelle instance de HashMap
     */
    public InventoryManager() {
        this.inventory = new HashMap<>();
    }

    /**
     * Ajoute un produit avec une quantité dans l'inventaire
     *
     * @param productId id du produit à ajouter
     * @param quantity La quantité du produits
     * @throws IllegalArgumentException Si qtt < 0
     */
    public void addProduct(String productId, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("quantité doit être supérieure à zéro.");
        }
        inventory.put(productId, inventory.getOrDefault(productId, 0) + quantity);
    }

    /**
     * Retire produit de l'inventaire
     *
     * @param productId id du produit à retirer
     * @param quantity quantité du produit à retirer
     * @throws IllegalArgumentException si quantité à retirer > dipso ou pas dans inventaire
     */
    public void removeProduct(String productId, int quantity) {
        if (!inventory.containsKey(productId)) {
            throw new IllegalArgumentException("Le produit n'est pas présent dans l'inventaire.");
        }
        int currentQuantity = inventory.get(productId);
        if (quantity > currentQuantity) {
            throw new IllegalArgumentException("La quantité à retirer est supérieure à la quantité disponible.");
        }
        inventory.put(productId, currentQuantity - quantity);
    }

    /**
     * Récupère stock produit dans inventaire
     *
     * @param productId id du produit
     * @return quantité dispo du produit si produit présent, sinon 0
     */
    public int getStockAvailability(String productId) {
        return inventory.getOrDefault(productId, 0);
    }
}
