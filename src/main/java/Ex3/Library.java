package Ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe library
 */
public class Library {
    List<Book> books;

    public Library() {
        this.books = new ArrayList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public boolean isBookAvailable(Book book) {
        return books.contains(book);
    }

    public void borrowBook(Book book, User user) {
        if (books.contains(book)) {
            books.remove(book);
            user.borrowBook(book);
        } else {
            System.out.println("livre pas dispo");
        }
    }
}

