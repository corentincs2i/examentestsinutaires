package ex1;

/**
 * Classe employee avec les noms, prenoms, niveau, annees
 */
public class Employee {
    private String firstName;
    private String lastName;
    private int Experience;
    private Level level;

    public Employee(String firstName, String lastName, int Experience, Level level) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.Experience = Experience;
        this.level = level;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return Experience;
    }

    public void setExperience(int Experience) {
        this.Experience = Experience;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public enum Level {
        JUNIOR,
        INTERMEDIAIRE,
        SENIOR
    }
}

