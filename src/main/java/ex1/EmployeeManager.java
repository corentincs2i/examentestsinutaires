package ex1;

/**
 * classe EmployeeManager Avec les salaires de base et multiplicateurs
 */
public class EmployeeManager {
	
	/**
	 * Salaries
	 * @param employee indicates level for salary
	 * @return salary
	 */
    public double calculateSalary(Employee employee) {
        double baseSalary;
        switch (employee.getLevel()) {
            case JUNIOR:
                baseSalary = 20000;
                break;
            case INTERMEDIAIRE:
                baseSalary = 40000;
                break;
            case SENIOR:
                baseSalary = 60000;
                break;
            default:
                throw new IllegalArgumentException("Niveau d'employé invalide: " + employee.getLevel());
        }
        
        //calculated salary
        double salary = baseSalary * calculateExperienceMultiplier(employee.getExperience());

        return salary;
    }

    /**
     * Experience calculator
     * @param anneesExperience multiplicated by coef
     * @return salary
     */
    public double calculateExperienceMultiplier(int anneesExperience) {
        double baseMultiplier = 1.0;
        double experienceMultiplier = baseMultiplier + (0.05 * anneesExperience);
        return experienceMultiplier;
    }
}

