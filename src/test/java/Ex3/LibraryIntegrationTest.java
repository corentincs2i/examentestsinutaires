package Ex3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * classe test pour l'emprunt de livres
 */
public class LibraryIntegrationTest {

    /**
     * teste si l'utilisateur peut emprunter un livre.
     */
    @Test
    public void testBorrowBook() {
        
        Library library = new Library();

        Book book = new Book("La quête d'Ewilan", "Pierre Bottero");

        library.addBook(book);

        User user = new User("Emeline");

        library.borrowBook(book, user);

        assertTrue(user.getBorrowedBooks().contains(book), "utilisateur emprunte livre");

        assertFalse(library.isBookAvailable(book), "livre empunté");
    }
}

