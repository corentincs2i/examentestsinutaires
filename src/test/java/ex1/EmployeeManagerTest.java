package ex1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

/**
 * Classe de tests pour EmployeeManager calculant des hypotèses de salaire pour l'ancienneté.
 */
public class EmployeeManagerTest {

	/**
     * Teste le calcul du salaire pour un employé junior avec 1 an d'ancienneté.
     * 
     * @param a Le salaire de base pour un employé junior.
     * @param b Le coefficient d'ancienneté pour un employé junior.
     */
    @Test
    public void testSalaryJuniorWith1YearExperience() {
        // Params
        double a = 20000; // salaire junior
        double b = 1.05;  // coef junior
        
        Employee employee = new Employee("Monsieur", "X", 1, Employee.Level.JUNIOR);
        
        EmployeeManager manager = new EmployeeManager();

        double actualSalary = manager.calculateSalary(employee);
        
        double expectedSalary = a * b;
        
        assertEquals(expectedSalary, actualSalary, 0.01, "salaire attendu junior 1 an xp");
    }
    
    /**
     * Teste le calcul du salaire pour un employé intermédiaire avec 5 ans d'ancienneté.
     * 
     * @param a Le salaire de base pour un employé intermédiaire.
     * @param b Le coefficient d'ancienneté pour un employé intermédiaire.
     */
    @Test
    public void testSalaryIntermediateWith5YearsExperience() {
        // Params
        double a = 40000; // salaire intermédiaire
        double b = 1.25;  // coef intermédiaire
        
        Employee employee = new Employee("Meidi", "Cament", 5, Employee.Level.INTERMEDIAIRE);
        
        EmployeeManager manager = new EmployeeManager();

        double actualSalary = manager.calculateSalary(employee);
        
        double expectedSalary = a * b;
        
        assertEquals(expectedSalary, actualSalary, 0.01, "salaire attendu intermediaire 5 ans d'xp");
    }
    
    /**
     * Teste le calcul du salaire pour un employé sénior avec 10 ans d'ancienneté.
     * 
     * @param a Le salaire de base pour un employé sénior.
     * @param b Le coefficient d'ancienneté pour un employé sénior.
     */
    @Test
    public void testSalarySeniorWith10YearsExperience() {
        // Params
        double a = 60000; // salaire sénior
        double b = 1.5;    // coef sénior
        
        Employee employee = new Employee("Alice", "Johnson", 10, Employee.Level.SENIOR);
        
        EmployeeManager manager = new EmployeeManager();

        double actualSalary = manager.calculateSalary(employee);
        
        double expectedSalary = a * b;
        
        assertEquals(expectedSalary, actualSalary, 0.01, "salaire attendu senior 10 ans d'xp");
    }
    
    /**
     * Teste le calcul du salaire pour un employé sénior avec 20 ans d'ancienneté.
     * 
     * @param a Le salaire de base pour un employé sénior.
     * @param b Le coefficient d'ancienneté pour un employé sénior.
     */
    @Test
    public void testSalarySeniorWith20YearsExperience() {
        // Params
        double a = 60000; // salaire sénior
        double b = 2.0;    // coef sénior
        
        Employee employee = new Employee("Lou", "Stique", 20, Employee.Level.SENIOR);
        
        EmployeeManager manager = new EmployeeManager();

        double actualSalary = manager.calculateSalary(employee);
        
        double expectedSalary = a * b;
        
        assertEquals(expectedSalary, actualSalary, 0.01, "salaire attendu senior 20 ans xp");
    }
    
    /**
     * Teste le calcul du salaire pour un employé junior avec 0 an d'ancienneté.
     * 
     * @param a Le salaire de base pour un employé junior.
     */
    @Test
    public void testSalaryJuniorWithZeroExperience() {
        // Params
        double a = 20000; // salaire junior
        
        Employee employee = new Employee("Eva", "Nescence", 0, Employee.Level.JUNIOR);
        
        EmployeeManager manager = new EmployeeManager();

        double actualSalary = manager.calculateSalary(employee);
        
        double expectedSalary = a * 1.0;
        
        assertEquals(expectedSalary, actualSalary, 0.01, "salaire attendu, junior pas d'xp");
    }
}

