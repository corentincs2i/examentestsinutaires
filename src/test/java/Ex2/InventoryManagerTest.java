package Ex2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class InventoryManagerTest {

    /**
     * premier paramètre  correspondant au nom du produit
     */
    private final String PRODUCT_A = "A";
    private final String PRODUCT_B = "B";
    private final String PRODUCT_C = "C";
    private final String PRODUCT_D = "D";

    /**
     * paramètre correspondant au stock
     */
    private final int STOCK_AVAILABLE_A = 100;
    private final int STOCK_AVAILABLE_B = 5;
    private final int STOCK_AVAILABLE_C = 0;

    /**
     * test produit en stock
     */
    @Test
    public void testSufficientStock() {
        InventoryManager manager = new InventoryManager();
        manager.addProduct(PRODUCT_A, STOCK_AVAILABLE_A);

        assertEquals(STOCK_AVAILABLE_A, manager.getStockAvailability(PRODUCT_A), "produit en stock");
    }

    /**
     * test produit stock insuffisant
     */
    @Test
    public void testInsufficientStock() {
        InventoryManager manager = new InventoryManager();
        manager.addProduct(PRODUCT_B, STOCK_AVAILABLE_B);

        assertEquals(STOCK_AVAILABLE_B, manager.getStockAvailability(PRODUCT_B), "produit avec peu de stock");
    }

    /**
     * test produit épuisé
     */
    @Test
    public void testOutOfStock() {
        InventoryManager manager = new InventoryManager();
        manager.addProduct(PRODUCT_C, STOCK_AVAILABLE_C);

        assertEquals(STOCK_AVAILABLE_C, manager.getStockAvailability(PRODUCT_C), "produit plus en stock");
    }

    /**
     * test produit pas dans inventaire
     */
    @Test
    public void testProductNotInInventory() {
        InventoryManager manager = new InventoryManager();

        assertThrows(IllegalArgumentException.class, () -> {
            manager.getStockAvailability(PRODUCT_D);
        }, "produit pas là");
    }
}
